"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {};
  },
  onLoad() {
    let token = common_vendor.index.getStorageSync("token");
    let openId = "wxc941c5303252b994";
    let secret = "669caf79e75a3670623530591e32d5b3";
    if (!token) {
      common_vendor.index.login({
        "provider": "weixin",
        "onlyAuthorize": true,
        success: function(loginRes) {
          common_vendor.index.request({
            url: `https://api.weixin.qq.com/sns/jscode2session?appid=${openId}&secret=${secret}&js_code=${loginRes.code}&grant_type=authorization_code`,
            data: {
              code: loginRes.code
            },
            success: (res) => {
              common_vendor.index.setStorageSync("token", res.token);
              console.log(res);
            }
          });
        },
        fail: function(err) {
          console.log(err.code);
        }
      });
    }
  },
  methods: {}
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {};
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/UniApp/apps/my_uniapp/pages/user/user.vue"]]);
wx.createPage(MiniProgramPage);
