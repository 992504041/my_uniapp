"use strict";
const common_vendor = require("../../common/vendor.js");
common_vendor.Ds.database();
const _sfc_main = {
  data() {
    return {
      pagesize: 10,
      where: "public==true"
    };
  },
  onPullDownRefresh() {
    this.$refs.udb.loadData({
      clear: true
      //可选参数，是否清空数据
    }, () => {
      common_vendor.index.stopPullDownRefresh();
    });
  },
  onReachBottom() {
    this.$refs.udb.loadMore();
  },
  onLoad: function() {
    this.$refs.udb.reset();
  },
  onShow() {
    this.$refs.udb.reset();
  },
  methods: {
    onqueryload(data, ended) {
    },
    onqueryerror(e) {
    },
    delnote(noteId) {
      common_vendor.Ds.callFunction({
        name: "fun",
        data: {
          api: "del",
          _id: noteId
        }
      }).then((res) => {
        if (res.success) {
          common_vendor.index.showToast({
            title: "删除成功",
            duration: 2e3
          });
        }
      });
    },
    upadatedata(noteId, messagecontent) {
      console.log("123");
      common_vendor.index.navigateTo({
        url: "/pages/notes/updateNote?id=" + noteId + "&data=" + messagecontent,
        animationType: "fade-in"
      });
    },
    moveToUpdatenNote(noteId, message) {
      common_vendor.index.navigateTo({
        url: "/pages/notes/updateNote?id=" + noteId + "&data=" + message,
        animationType: "fade-in"
      });
    }
  }
};
if (!Array) {
  const _easycom_unicloud_db2 = common_vendor.resolveComponent("unicloud-db");
  _easycom_unicloud_db2();
}
const _easycom_unicloud_db = () => "../../node-modules/@dcloudio/uni-components/lib/unicloud-db/unicloud-db.js";
if (!Math) {
  _easycom_unicloud_db();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.w(({
      data,
      loading,
      pagination,
      error,
      hasMore
    }, s0, i0) => {
      return common_vendor.e({
        a: error
      }, error ? {
        b: common_vendor.t(error.message)
      } : loading ? {} : {
        d: common_vendor.f(data, (note, k1, i1) => {
          return {
            a: common_vendor.t(note.time),
            b: common_vendor.t(note.message),
            c: common_vendor.o(($event) => $options.upadatedata(note._id, note.message), note._id),
            d: common_vendor.o(($event) => $options.delnote(note._id), note._id),
            e: note._id
          };
        }),
        e: loading,
        f: error.message || "没有更多数据"
      }, {
        c: loading,
        g: i0,
        h: s0
      });
    }, {
      name: "d",
      path: "a",
      vueId: "69d06855-0"
    }),
    b: common_vendor.sr("udb", "69d06855-0"),
    c: common_vendor.o($options.onqueryload),
    d: common_vendor.o($options.onqueryerror),
    e: common_vendor.p({
      collection: "messageTable",
      orderby: "time desc",
      options: _ctx.options,
      ["page-data"]: "replace",
      field: "message,time",
      getone: false,
      action: _ctx.action,
      where: $data.where,
      showToast: "true",
      ["page-size"]: $data.pagesize
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/UniApp/apps/my_uniapp/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
