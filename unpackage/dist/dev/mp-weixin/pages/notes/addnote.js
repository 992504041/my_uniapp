"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      date: this.getDate(),
      title: "时间，是最好的老师!",
      textareaValue: "",
      isInput: this.textareaValue ? true : false,
      haveNewData: false,
      isUpate: false
    };
  },
  onLoad: function(option) {
    this.getDate();
  },
  methods: {
    publish() {
      if (this.textareaValue) {
        common_vendor.Ds.callFunction({
          name: "fun",
          data: {
            api: "publish",
            message: this.textareaValue,
            time: /* @__PURE__ */ new Date()
          }
        }).then((res) => {
          console.log(res);
          this.textareaValue = "";
          common_vendor.index.showToast({
            title: "发布成功",
            duration: 2e3,
            success: function() {
              common_vendor.index.$emit("refresh", { refresh: true });
              common_vendor.index.switchTab({
                url: "/pages/index/index",
                success: function() {
                }
              });
            }
          });
        });
      } else {
        common_vendor.index.showModal({
          title: "请输入你想说的话"
        });
        return;
      }
    },
    getDate() {
      let date = /* @__PURE__ */ new Date();
      let year = date.getFullYear();
      let month = date.getMonth() + 1;
      let day = date.getDate();
      this.date = year + "年" + month + "月" + day + "日";
      let week = date.getDay();
      switch (week) {
        case 0:
          this.date += " 星期日";
          break;
        case 1:
          this.date += " 星期一";
          break;
        case 2:
          this.date += " 星期二";
          break;
        case 3:
          this.date += " 星期三";
          break;
        case 4:
          this.date += " 星期四";
          break;
        case 5:
          this.date += " 星期五";
          break;
        case 6:
          this.date += " 星期六";
          break;
      }
      return this.date;
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.t($data.date),
    b: common_vendor.t($data.title),
    c: common_vendor.o(($event) => $data.isInput = true),
    d: common_vendor.o(($event) => $data.isInput = false),
    e: $data.textareaValue,
    f: common_vendor.o(($event) => $data.textareaValue = $event.detail.value),
    g: $data.isInput || !$data.haveNewData
  }, $data.isInput || !$data.haveNewData ? {
    h: common_vendor.o((...args) => $options.publish && $options.publish(...args))
  } : {}, {
    i: $data.haveNewData
  }, $data.haveNewData ? {
    j: common_vendor.o((...args) => $options.publish && $options.publish(...args))
  } : {});
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/UniApp/apps/my_uniapp/pages/notes/addnote.vue"]]);
wx.createPage(MiniProgramPage);
