"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      noteId: "",
      textareaValue: ""
    };
  },
  onLoad: function(option) {
    if (option) {
      this.noteId = option.id;
      this.textareaValue = option.data;
    }
  },
  methods: {
    updateNote() {
      if (this.textareaValue) {
        common_vendor.Ds.callFunction({
          name: "fun",
          data: {
            api: "update",
            _id: this.noteId,
            newValue: this.textareaValue
          }
        }).then((res) => {
          if (res.success) {
            common_vendor.index.showToast({
              title: "修改完成",
              duration: 2e3,
              success: function() {
                common_vendor.index.navigateBack({
                  delta: 1
                });
              }
            });
          }
        });
      }
      return 0;
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: $data.textareaValue,
    b: common_vendor.o(($event) => $data.textareaValue = $event.detail.value),
    c: common_vendor.o((...args) => $options.updateNote && $options.updateNote(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/UniApp/apps/my_uniapp/pages/notes/updateNote.vue"]]);
wx.createPage(MiniProgramPage);
