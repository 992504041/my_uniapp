let db = uniCloud.database({
	 throwOnNotFound: false,
})
exports.main = async (event, context) => {
	//event为客户端上传的参数
	if(event.api==="publish") {
		return await db.collection("messageTable").add({
			message: event.message,
			time: event.time,
			public: false
		})
	}
	if(event.api==="getNotes") {
		return await db.collection("messageTable").orderBy("time", "desc").where({
			public: true
		}).get()
	}
	if(event.api==="del") {
		return await db.collection("messageTable").doc(event._id).remove()
	}
	if(event.api==="update") {
		return await db.collection("messageTable").doc(event._id).update({
		  message: event.newValue
		})
	}
	if(event.api==="getNotesById") {
		return await db.collection("messageTable").doc(event._id).get()
	}
};


